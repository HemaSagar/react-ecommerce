import React from "react";

class ListItem extends React.Component {
    delete = () => {
        this.props.onDelete(this.props.item.id);
    }

    increment = () => {
        this.props.onIncrement(this.props.item.id);
    }
    decrement =() =>{
        this.props.onDecrement(this.props.item.id,this.props.quantity);
    }
    render() {
        let item = this.props.item;
        return (
            <div className="cart-item">
                <div className="cart-item-image">
                    <img src={item.image} alt="product"></img>
                </div>
                <div className="item-details">
                    <h4>{item.title}</h4>
                    <div className="price-quantity">
                        <div className="quantity">
                        <span className="change">
                            <button className="decrement" onClick={()=>{this.decrement()}}>-</button>
                        </span>
                        <span className="quantity-text">{this.props.quantity}</span>
                        <span className="change">
                            <button className="increment" onClick={()=>{this.increment()}}>+</button>
                        </span>
                        </div>
                        <div className="price">
                            <span>$ </span><span>{this.props.price}</span>
                        </div>
                    </div>
                </div>
                <div className="delete">
                    <button onClick={()=>{this.delete()}}>Delete</button>
                </div>
            </div>
        );
    }
}

export default ListItem;