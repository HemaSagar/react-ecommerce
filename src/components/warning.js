import React from "react";

class Warning extends React.Component{
    clearEditing = () => {
        this.props.onYes();
    }
    cancelClose = () => {
        this.props.onNo();
    }
    render () {
        return (
            <div className="warning-wrapper">

            <div className="warning">
                <h3>You will loose your progress, still want to continue?</h3>
                <div className="warning-buttons">
                    <button onClick={this.clearEditing}>Yes</button>
                    <button onClick={this.cancelClose}>No</button>
                </div>
            </div>
            </div>
        );
    }
}

export default Warning;