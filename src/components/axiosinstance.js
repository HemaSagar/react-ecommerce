import axios from "axios";

const productsAxios = axios.create({
    baseURL: "https://fakestoreapi.com/"
})


export default productsAxios;