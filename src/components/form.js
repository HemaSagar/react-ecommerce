import React from "react";

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.productData,
            valid:false,
            titleError:false,
            priceError:false,
            categoryError:false,
            descriptionError:false
        }
    }
    change = (event,validator,error) => {
        this.setState({
            data: { ...this.state.data, [event.target.name]: event.target.value },
            [error]:validator(event.target.value)
        })
    }

    formSubmit = (event) => {
        event.preventDefault();
        if(!this.state.titleError && !this.state.priceError && !this.state.categoryError && !this.state.descriptionError)
        {
            this.props.updateInOriginal(this.state.data);
        }
    }

    closeForm = (event) => {
        event.preventDefault();
        this.props.showWarning();
    }

    validateTitle = (value) => {
        if (value.trim().length>0){
            return false;
        }
        else{
            return true;
        }
    }

    validatePrice = (value) => {
        const regex = new RegExp(/^(?:[1-9][0-9]{0,4}(?:\.\d{1,2})?|100000|100000.00)$/)
        if(!regex.test(value)){
            return true;

        }
        // if(isNaN(Number(value)) || value.length===0 || value.includes(' ')){
        //     return true;
        // }
        else{
            return false;
        }
    }

    validateCategory = (value) => {
        if (value.trim().length>0){
            return false;
        }
        else{
            return true;
        }
    }

    validateDescription = (value) => {
        if (value.trim().length>0){
            return false;
        }
        else{
            return true;
        }
    }

    render() {
        return (
            <div className="edit-details">
                <h1>Edit Details</h1>
                <form className="update-form">
                    <div className="input-fields">

                    <label >Title</label>
                    <input type={"text"} id="title" name="title" value={this.state.data.title} onChange={(event) => { this.change(event,this.validateTitle,"titleError") }} />
                        {this.state.titleError ? <div style={{color:"red",fontSize:"11px"}}>Title must not be empty</div> :null}
                    </div>
                    <div className="input-fields">
                    <label >Price</label>
                    <input type={"text"} id="price" name="price" value={this.state.data.price} onChange={(event) => { this.change(event,this.validatePrice,"priceError") }} />
                    {this.state.priceError ? <div style={{color:"red",fontSize:"11px"}}>Price must be a number between 1 and 100000 and max 2 decimals</div> :null}
                         </div>
                    <div className="input-fields">

                    <label >Category</label>
                    <input type={"text"} id="category" name="category" value={this.state.data.category} onChange={(event) => { this.change(event,this.validateCategory,"categoryError") }} />
                    {this.state.categoryError ? <div style={{color:"red",fontSize:"11px"}}>Category must not be empty</div> :null}
                    </div>
                    <div className="input-fields">

                    <label >Description</label>
                    <textarea id="description" name="description" value={this.state.data.description} onChange={(event) => { this.change(event,this.validateDescription,"descriptionError") }} />
                    {this.state.descriptionError ? <div style={{color:"red",fontSize:"11px"}}>Description must not be empty</div> :null}
                    </div>
                    <div className="form-buttons">
                        <button className="cancel-button" onClick={this.closeForm}>Cancel</button>
                        <input className="update-button" type={"submit"} value="Update" onClick={this.formSubmit} />
                    </div>
                </form>
            </div>
        );
    }
}

export default Form;