import React from "react";

class RunningPopup extends React.Component{
    closePopUp = () => {
        this.props.onOk();
    }
    render(){
        return (
            <div className="popup-wrapper">

            <div className="running-popup">
                <h3>Please cancel or update current progress</h3>
                <button onClick={this.closePopUp}>Ok</button>
            </div>
            </div>
        )
    }
}

export default RunningPopup