import React from "react";
import productsAxios from "./axiosinstance";
import Loader from "./loader";

class SingleProduct extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            product: null,
            error: null,
        }
    }

    errorHandler = (error) => {
        if (error instanceof Error) {
            return <h2 className="error">Failed to load product</h2>
        }
        else if (error === "no data") {
            return <h2 className="error">Sorry product not available at the moment, come back later</h2>
        }
    }

    componentDidMount = () => {
        productsAxios.get(`/products/${this.props.id}`)
            .then((response) => {
                this.setState({ product: response.data, error: null });

            })
            .catch((error) => {
                this.setState({ product: null, error: error })
            })
    }

    addCart = (product) => {
        this.props.addToCart(product);
    }

    render() {
        let renderElement = <div className="single-product" ><Loader /></div>;
        if (this.state.error === null && this.state.product === null) {
            return renderElement;
        }
        if (this.state.error) {
            renderElement = this.errorHandler(this.state.error);
        }
        if (this.state.product && Object.keys(this.state.product).length === 0) {
            renderElement = this.errorHandler("no data");
        }
        if (this.state.product && Object.keys(this.state.product).length > 0) {
            let cartQuantity = 0;
            let product = this.state.product;

            let currIndex = this.props.cartItems.findIndex((item) => {
                return Object.keys((item))[0] == product.id;
            })
            if (this.props.cartItems[currIndex]) {
                cartQuantity = this.props.cartItems[currIndex][product.id].quantity;
            }
            renderElement = (<div className="single-product" >
                <div className="single-prod-image">
                    <img src={this.state.product.image} alt="product"></img>
                </div>
                <div className="single-prod-details">
                    <h1>{this.state.product.title}</h1>
                    <p className="single-prod-rating"><span>{'\u2606'}</span>{this.state.product.rating.rate}/5 <span>({this.state.product.rating.count})</span></p>
                    <p className="single-prod-price"><span>{'\u0024'}</span> {this.state.product.price}</p>
                    <p className="single-prod-description">{this.state.product.description}</p>
                    <button className="add-to-cart" onClick={() => { this.addCart(this.state.product) }}>{cartQuantity > 0 ? "Remove from Cart" : "Add to Cart"}</button>

                </div>
            </div>);
        }

        return renderElement;

    }
}

export default SingleProduct;