import React from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import ListItem from "./cartItem";

class Cart extends React.Component {
    render() {
        if (this.props.cartItems.length === 0) {
            return (<div className="cart-items-container">
                <div className="no-items-info">
                    <h2 style={{ textAlign: "center" }}>No items in the cart, add proucts to cart</h2>
                    <Link to="/">
                        <p>View all products</p>
                    </Link>
                </div>
            </div>);
        }
        return (
            <div className="cart-items-container">
                {this.props.cartItems.map((item) => {
                    let key = Object.keys(item)[0];
                    let values = Object.values(item[key]);
                    return (
                        <ListItem item={values[0]} quantity={values[1]} onDelete={this.props.onDelete} onIncrement={this.props.onIncrement} onDecrement={this.props.onDecrement} price={values[2]} key={key} />
                    )
                })}
            </div>
        );
    }
}

export default Cart;