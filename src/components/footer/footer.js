import React from "react";

class Footer extends React.Component {
    render() {
        return (
            <footer>
                    <p>About us</p>
                    <p>Contact us</p>
                    <p>© Eazy Cart,Inc. All Rights Reserved</p>
            </footer>
        );
    }
}

export default Footer