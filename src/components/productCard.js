import React from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";

class ProductCard extends React.Component {


    handleEdit = (productDetails) => {
        if(this.props.selected){
            this.props.showRunning();
        }
        this.props.enterEdit(productDetails)
    }
    
    render() {
        const product = this.props.product
            return(
                // <div className="product-wrapper">
                // <div className="product-card" id="product-card" onClick={(event)=>{this.handleEdit(this.props.product)}}>
                <div className="product-card" id="product-card">
                <Link to={"/singleProduct/"+product.id} style={{paddingLeft: 13, textDecoration: 'none', color:"inherit"}}>
                    <div className="prod-image">
                        <img src={product.image} alt="product-img"/>
                    </div>
                    <div className="info">
                        <h4 className="text">{product.title}</h4>
                        <p className="prod-rating"><span>{'\u2606'}</span>{product.rating.rate}/5 <span>({product.rating.count})</span></p>
                        <p className="prod-price text"><span>{'\u0024'}</span> {product.price}</p>
                        <p className="prod-description text">{product.description.slice(0, 100) + "..."}</p>
                    </div>
                </Link>
                <button className="edit-details-button" onClick={(event)=>{event.stopPropagation();this.handleEdit(this.props.product)}}>Edit Details</button>
                </div>
                // </div>
            );
    }
}

export default ProductCard