import React from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";

class Header extends React.Component {

    render() {
        return (
            <header className="header-container">
                <div className="logo">
                    <Link to='/'>
                        <img src="logo.png" alt="logo" />
                    </Link>
                </div>
                <nav className="nav-container">
                    <ul>
                        <li>
                            <Link to='/' className="anchor">
                                Home
                            </Link>
                        </li>
                        <li>
                            <Link to='/cart' className="anchor">
                                Cart
                            </Link>
                        </li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Header;