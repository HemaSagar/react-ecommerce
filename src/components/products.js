import React from "react";
import Loader from "./loader";
import ProductCard from "./productCard";
import Editor from "./editor";
import productsAxios from "./axiosinstance";
import Warning from "./warning";
import RunningPopup from "./runningPopup";

class Products extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: null,
            error: null,
            productSelected: false,
            prodData: null,
            warning:false,
            running:false

        }
    }

    componentDidMount = () => {
        productsAxios.get("products")
            .then((response) => {
                this.setState({ products: response.data, error: null });

            })
            .catch((error) => {
                this.setState({ products: null, error: error })
            })
    }

    errorHandler = (error) => {
        if (error instanceof Error) {
            return <h2 className="error">Failed to load products</h2>
        }
        else if (error === "no products") {
            return <h2 className="error">Sorry no products available at the moment, come back later</h2>
        }
    }

    editProduct = (prodDetails) => {

        this.setState({
            productSelected: true,
            prodData: prodDetails
        })
    }

    updateInOriginal = (updatedData) => {
        let updatedIndex = this.state.products.findIndex((item, index) => {
            return item.id === updatedData.id;
        })

        let copyAll = this.state.products;
        copyAll[updatedIndex] = updatedData;
        this.setState({ products: copyAll ,productSelected:false});
    }

    showWarning = () => {
        this.setState({warning:true})
    }
    onYes = ()=> {
        this.setState({warning:false, productSelected:false});
    }
    onNo =() => {
        this.setState({warning:false},);
    }

    showRunningPopUp = () => {
        this.setState({running:true});
    }

    onOk = () => {
        this.setState({running:false})
    }

    render() {
        let renderElement = (<div className="products-edit"><Loader /></div>);
        if (this.state.error === null && this.state.products === null) {
            return renderElement;
        }
        if (this.state.error) {
            renderElement = this.errorHandler(this.state.error);
        }
        if (this.state.products && this.state.products.length === 0) {
            renderElement = this.errorHandler("no products");
        }
        if (this.state.products && this.state.products.length > 0) {
            renderElement = this.state.products.map((product) => {
                return <ProductCard product={product} selected={this.state.productSelected} showRunning ={this.showRunningPopUp} key={product.id} enterEdit={this.editProduct} />
            })
        }


        return (
            <div className="products-edit">
                <div className="products-container">
                    {renderElement}
                </div>
                {this.state.warning && <Warning onYes={this.onYes} onNo={this.onNo} />}
                {this.state.running && <RunningPopup onOk = {this.onOk}/>}
                <div className="editor">
                    <Editor productSelected={this.state.productSelected} productData={this.state.prodData} updateInOriginal={this.updateInOriginal} showWarning={this.showWarning} />
                </div>
            </div>
        )
    }
}

export default Products