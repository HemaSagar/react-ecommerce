import React from "react";
import Form from "./form";

class Editor extends React.Component {
    render() {
        if (!this.props.productSelected ) {
            return <h2 className="select-message">Select any one product to edit</h2>
        }
        return (
            <Form  productData={this.props.productData} updateInOriginal={this.props.updateInOriginal} showWarning={this.props.showWarning}/>
        )
    }
}

export default Editor