import React from 'react';
import './App.css';
import Header from './components/header/header';
import Products from './components/products';
import Footer from './components/footer/footer';
import { Route } from 'react-router-dom/cjs/react-router-dom.min';
import SingleProduct from './components/singleProduct';
import Cart from './components/cart';

class App extends React.Component {

  constructor() {
    super();
    this.state = {
      cartItems: []
    }
  }

  addToCart = (product, itemState) => {
    let currIndex = this.state.cartItems.findIndex((item) => {
      return Object.keys((item))[0] == product.id;
    })

    if (currIndex === -1) {
      let copyCart = [...this.state.cartItems, { [product.id]: { product: { ...product }, quantity: 1, price: product.price } }];
      this.setState({
        cartItems: copyCart
      })
    }
    else {
      let copyCart = [...this.state.cartItems];
      copyCart.splice(currIndex, 1);
      this.setState({ cartItems: copyCart })
    }
  }

  onDelete = (itemId) => {
    let currIndex = this.state.cartItems.findIndex((item) => {
      return Object.keys((item))[0] == itemId;
    })
    let copyCart = [...this.state.cartItems];
    copyCart.splice(currIndex, 1);
    this.setState({ cartItems: copyCart })

  }

  onIncrement = (itemId) => {
    let currIndex = this.state.cartItems.findIndex((item) => {
      return Object.keys((item))[0] == itemId;
    })
    let copyCart = JSON.parse(JSON.stringify(this.state.cartItems));
    copyCart[currIndex][itemId].price = (copyCart[currIndex][itemId].price + this.state.cartItems[currIndex][itemId].product.price)
    copyCart[currIndex][itemId].quantity = (copyCart[currIndex][itemId].quantity + 1);

    this.setState({ cartItems: copyCart });
  }

  onDecrement = (itemId,currQuantity) => {
    let currIndex = this.state.cartItems.findIndex((item) => {
      return Object.keys((item))[0] == itemId;
    })
    let copyCart = JSON.parse(JSON.stringify(this.state.cartItems));

    if(currQuantity===1){
      copyCart.splice(currIndex, 1);
    this.setState({ cartItems: copyCart })
    }
    else{
    copyCart[currIndex][itemId].price = (copyCart[currIndex][itemId].price - this.state.cartItems[currIndex][itemId].product.price).toFixed(2)
    copyCart[currIndex][itemId].quantity = (copyCart[currIndex][itemId].quantity - 1);
    this.setState({ cartItems: copyCart });
    }
  }

  render() {
    return (
      <div className='container'>
        <Header />
        <Route path='/' exact render={() => {
          return <Products />
        }} />
        <Route path='/singleProduct/:id' render={(props) => (
          <SingleProduct id={props.match.params.id} addToCart={this.addToCart} cartItems={this.state.cartItems} />
        )} />
        <Route path='/cart' render={(props) => {
          return <Cart cartItems={this.state.cartItems} onDelete={this.onDelete} onIncrement={this.onIncrement} onDecrement={this.onDecrement}/>
        }} />
        <Footer />
      </div>
    );
  }
}

export default App;
